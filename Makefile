EMACS = emacs -Q -q --batch -nw --eval "(package-initialize)"
EMACS_COMPILE = -f emacs-lisp-byte-compile
EMACS_DIR = ~/.emacs.d/marvin/

all: marvin.elc

%.elc: %.el
	$(info Compile $@)
	@$(EMACS) $< $(EMACS_COMPILE)

install: marvin.elc
	$(info Install)
	@mkdir -p $(EMACS_DIR)
	@cp -v *.el  $(EMACS_DIR)
	@cp -v *.elc $(EMACS_DIR)
	@cp -v LICENSE $(EMACS_DIR)
	@cp -v logo.png $(EMACS_DIR)

.PHONY: clean
clean:
	rm -v *.elc
