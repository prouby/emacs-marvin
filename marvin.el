;;; marvin.el --- Marvin the depressed Emacs mode    -*- lexical-binding: t; -*-

;; Copyright (C) 2019  Pierre-Antoine Rouby

;; Author: Pierre-Antoine Rouby <contact@parouby.fr>
;; Keywords: extensions

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; Marvin the depressed Emacs mode... Run `marvin-on' with log file
;; and get notification when this file change.

;;; Code:

(require 'notifications)

(defvar marvin-continue nil)
(defvar marvin-timer nil)
(defvar marvin-index 0)
(defvar marvin-quote
  '("La vie... ne me parlez pas de la vie."
    "Je sais. Affreux, n'est-ce pas ?"
    "Je vous avais dit que ça finirait dans les larmes."
    "Je peux calculer vos chances de survie, mais ça ne vous plaira pas."
    "Il ferait bon que vous sachiez que je suis très déprimé."
    "J'ai un million d'idées. Elles ont toutes la mort comme toile de fond."))

(defvar marvin-quote-nb (length marvin-quote))

(defvar marvin-logo (concat (file-name-directory load-file-name)
                            "logo.png"))

(defvar marvin-files '())

(defun marvin-notif (title message)
  (notifications-notify :title title
                        :body message
                        :app-name "emacs-marvin"
                        :app-icon marvin-logo))

(defun marvin-check-file-size (file-lst)
  (let ((file (nth 0 file-lst))
        (size (nth 1 file-lst)))
    (let* ((size2   (file-attribute-size (file-attributes file)))
           (changed (not (eq size size2))))
      (when changed
        (marvin-notif "Marvin" (concat (nth marvin-index marvin-quote)
                                       " (" file ")"))
        (setq marvin-index (mod (+ 1 marvin-index) marvin-quote-nb)))
      (if marvin-continue
          (list file size2)
        nil))))

(defun marvin ()
  (setq marvin-files (remove 'nil (mapcar 'marvin-check-file-size marvin-files))))

;;;###autoload
(defun kill-marvin ()
  (interactive)
  (setq marvin-files '())
  (setq marvin-continue nil)
  (marvin-notif "Marvin" "J'ai un mal de tête maintenant.")
  (cancel-timer marvin-timer)
  (setq marvin-timer nil))

;;;###autoload
(defun marvin-on (file)
  (interactive "fFichier à surveiller : ")
  (setq marvin-continue t)
  (add-to-list 'marvin-files (list file (file-attribute-size (file-attributes file))))
  (if (not marvin-timer)
      (setq marvin-timer (run-with-timer 1 1 'marvin)))
  (marvin-notif "Marvin" "C'est tout ? Ça ne me plaira pas."))

(provide 'marvin)
;;; marvin.el ends here
